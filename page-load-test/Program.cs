﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;

namespace page_load_test
{
    public class Program
    {
        private static string url = ConfigurationManager.AppSettings["url"];
        private static string username = ConfigurationManager.AppSettings["username"];
        private static string password = ConfigurationManager.AppSettings["password"];
        private static int iterations = int.Parse(ConfigurationManager.AppSettings["iterations"]);

        public static void Main(string[] args)
        {
            var stopwatch = Stopwatch.StartNew();
            Parallel.For(0, iterations, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, BenchmarkPage);
            stopwatch.Stop();
            Console.WriteLine("Average requests per second: {0:0.00}", (float)stopwatch.ElapsedMilliseconds / 1000 / iterations);
            Console.WriteLine("Total time elapsed: {0:hh\\:mm\\:ss}", stopwatch.Elapsed);
        }

        private static void BenchmarkPage(int x)
        {
            using (var driver = new PhantomJSDriver())
            {
                Console.WriteLine("Opening page...");
                driver.Navigate().GoToUrl(url);
                driver.FindElementByName("username").SendKeys(username);
                driver.FindElementByName("password").SendKeys(password);
                driver.FindElementByName("password").SendKeys(Keys.Enter);
                Thread.Sleep(2000);
                Console.WriteLine("Complete.");
            }
        }
    }
}
